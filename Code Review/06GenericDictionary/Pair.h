#ifndef INC_06GENERICDICTIONARY_PAIR_H
#define INC_06GENERICDICTIONARY_PAIR_H

#include <sstream>

template <typename K, typename V>
class Pair {
public:
    Pair(){}
    Pair (K key, V value){
        _key = key;
        _value = value;
    }

    std::stringstream printPair() {
        std::stringstream ss;
        ss << _key << " " << _value;
        return ss;
    }

    K getKey() { return _key; }

    V getValue() { return _value; }

private:
    K _key;
    V _value;
};

#endif //INC_06GENERICDICTIONARY_PAIR_H
