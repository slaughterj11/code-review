#ifndef INC_06GENERICDICTIONARY_DICTIONARY_H
#define INC_06GENERICDICTIONARY_DICTIONARY_H


#include <vector>
#include <iostream>
#include "Pair.h"

template <typename K, typename V>
class Dictionary {
public:
    Dictionary(int){}
    Dictionary() {};
    Dictionary(K key, V value) { dictionary.push_back(Pair<K,V>(key, value)); }

    void add(K key, V value){
        if (!isDuplicate(key)) dictionary.push_back(Pair<K,V>(key, value));
    }

    bool isDuplicate(K key){
        for (int i = 0; i < (int)dictionary.size(); ++i)
            if (dictionary[i].getKey() == key) return true;
        return false;
    }

    Pair<K,V> getByIndex(int n){
        Pair<K,V> temp;
        if (n >= 0 && n < dictionary.size())
            return dictionary[n];
        else
            std::cout << "Out of dictionary range\n\n";
        return temp;
    }

    void removeByKey(K key){
        for (int i = 0; i < dictionary.size(); ++i)
            if (dictionary[i].getKey() == key) {
                dictionary.erase(dictionary.begin() + i);
                return;
            }
        std::cout << "Key not found, not removed\n\n";
    }

    void removeByIndex(int n){
        if (n >= 0 && n < dictionary.size())
            dictionary.erase(dictionary.begin() + n);
        else
            std::cout << "Out of dictionary range, not removed\n\n";
    }

    int getSize() { return (int)dictionary.size(); }

    void print(){
        for (int i = 0; i < dictionary.size(); ++i)
            std::cout << dictionary[i].printPair().str() << std::endl;
        std::cout << std::endl;
    }

    Pair<K,V> getByKey(K key) {
        Pair<K,V> temp;
        for (int i = 0; i < dictionary.size(); ++i)
            if (dictionary[i].getKey() == key) return dictionary[i];
        return temp;
    }

private:
    std::vector<Pair<K,V>> dictionary;
};



#endif //INC_06GENERICDICTIONARY_DICTIONARY_H
