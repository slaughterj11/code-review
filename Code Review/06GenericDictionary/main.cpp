#include <iostream>
#include "Pair.h"
#include "Dictionary.h"


void printInstructions(){
    std::cout << "Dictionary Functions\n"
              << "K = Key, V = Value\n"
              << "add(K, V) - add new type to dictionary\n"
              << "getByIndex(int) - returns a key/value by given index\n"
              << "getByKey(K) - returns a key/value by key\n"
              << "removeByKey(K) - take template key and removes it from dictionary\n"
              << "removeByIndex(int) - removes a key/value by given index\n"
              << "getSize() - returns size of dictionary\n"
              << "print() - prints whole dictionary\n"
              << "\nPair Functions\n"
              << "printPair() - prints the key/value\n"
              << "getKey() - returns key\n"
              << "getValue() - return value\n";
}

int main() {
    Dictionary<std::string, int> list;
    Pair<std::string, int> p;


    printInstructions();


    std::cout << "\n\nexample starts here\n";

    list.add("first", 1);
    list.add("second", 2);
    list.add("third", 3);
    list.add("fourth", 4);
    p = list.getByKey("first");

    list.print();

    list.removeByKey("second");
    list.removeByIndex(0);

    list.print();

    list.add("first", 1);

    list.print();

    return 0;
}