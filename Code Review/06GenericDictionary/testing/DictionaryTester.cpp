#include <iostream>
#include "DictionaryTester.h"
#include "../Dictionary.h"

void DictionaryTester::testConstructors() {
    std::cout << "Testing Constructor:";
    {
        Dictionary<int, int> list(1,2);
        if (list.getSize() != 1) {
            std::cout << "\n\tConstructor is failing\n";
            return;
        }
    }
    {
        Dictionary<std::string, int> list("test",1);
        if (list.getSize() != 1) {
            std::cout << "\n\tString Constructor is failing\n";
            return;
        }
    }
    std::cout << " SUCCESS\n";
}

void DictionaryTester::testAdd() {
    std::cout << "Testing Add function:";
    {
        Dictionary<int, int> list;
        list.add(1,2);
        list.add(2,3);
        if (list.getSize() != 2) {
            std::cout << "\n\tAdding is failing\n";
            return;
        }
    }
    {
        Dictionary<int, int> list(1,2);
        list.add(2,3);
        if (list.getSize() != 2) {
            std::cout << "\n\tAdding is failing\n";
            return;
        }
    }
    std::cout << " SUCCESS\n";
}

void DictionaryTester::testDuplicate() {
    std::cout << "Testing duplicates:";
    {
        Dictionary<int, int> list(1,2);
        list.add(1,2);
        if (list.getSize() != 1) {
            std::cout << "\n\tDuplicate is failing\n";
            return;
        }
    }
    std::cout << " SUCCESS\n";
}

void DictionaryTester::testGet() {
    std::cout << "Testing getters:";
    {
        Dictionary<int, int> list(1,2);
        Pair<int, int> p;
        if (list.getSize() != 1) {
            std::cout << "\n\tGet size is failing\n";
            return;
        }
        p = list.getByIndex(0);
        if (p.getKey() != 1) {
            std::cout << "\n\tGet by index is failing\n";
            return;
        }
        p = list.getByKey(1);
        if (p.getKey() != 1) {
            std::cout << "\n\tGet by key is failing\n";
            return;
        }
    }
    std::cout << " SUCCESS\n";
}

void DictionaryTester::testRemove() {
    std::cout << "Testing remove:";
    {
        Dictionary<int, int> list(1,2);
        list.removeByIndex(0);
        if (list.getSize() != 0) {
            std::cout << "\n\tRemove by index is failing\n";
            return;
        }
    }
    {
        Dictionary<int, int> list(1,2);
        list.removeByKey(1);
        if (list.getSize() != 0) {
            std::cout << "\n\tRemove by key is failing\n";
            return;
        }
    }
    std::cout << " SUCCESS\n";
}