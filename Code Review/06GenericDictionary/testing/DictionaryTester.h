#ifndef INC_06GENERICDICTIONARY_DICTIONARYTESTER_H
#define INC_06GENERICDICTIONARY_DICTIONARYTESTER_H

class DictionaryTester {
public:
    void testConstructors();
    void testAdd();
    void testDuplicate();
    void testGet();
    void testRemove();
};


#endif //INC_06GENERICDICTIONARY_DICTIONARYTESTER_H
