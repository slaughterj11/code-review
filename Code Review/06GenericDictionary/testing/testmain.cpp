#include <iostream>
#include "DictionaryTester.h"
#include "PairTester.h"

int main() {

    std::cout << "Testing\n\n";

    DictionaryTester dictionaryTester;
    PairTester pairTester;

    dictionaryTester.testConstructors();
    dictionaryTester.testAdd();
    dictionaryTester.testDuplicate();
    dictionaryTester.testGet();
    dictionaryTester.testRemove();

    std::cout << std::endl;

    pairTester.testConstructor();
    pairTester.testGetKey();
    pairTester.testGetValue();

    return 0;
}