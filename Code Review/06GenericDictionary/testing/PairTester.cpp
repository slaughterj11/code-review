#include <iostream>
#include "PairTester.h"
#include "../Pair.h"


void PairTester::testConstructor() {
    std::cout << "Testing Constructor: ";
    {
        Pair<int, int> pair(1,2);
        if (pair.getKey() != 1) {
            std::cout << "\n\tConstructor is failing\n";
            return;
        }
    }
    std::cout << "SUCCESS\n";
}

void PairTester::testGetKey() {
    std::cout << "Testing getKey: ";
    {
        Pair<int, int> pair(1,2);
        if (pair.getKey() != 1) {
            std::cout << "\n\tGet key is failing\n";
            return;
        }
    }
    std::cout << "SUCCESS\n";
}

void PairTester::testGetValue() {
    std::cout << "Testing getValue: ";
    {
        Pair<int, int> pair(1,2);
        if (pair.getKey() != 1) {
            std::cout << "\n\tGet value is failing\n";
            return;
        }
    }
    std::cout << "SUCCESS\n";
}