#ifndef INC_06GENERICDICTIONARY_PAIRTESTER_H
#define INC_06GENERICDICTIONARY_PAIRTESTER_H

class PairTester {
public:
    void testConstructor();
    void testGetKey();
    void testGetValue();
};

#endif //INC_06GENERICDICTIONARY_PAIRTESTER_H
